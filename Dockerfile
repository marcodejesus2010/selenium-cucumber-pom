FROM ubuntu:trusty

# Create folder structure move the Automation Project to Container.
RUN mkdir /home/ruby
WORKDIR /home/ruby/
ADD . /home/ruby/

# This file will have cucumber instructions. ie.: cucumber features/tests/login.feature -f pretty -f html -o report.html
RUN chmod a+x runner.sh

# Then we need to get required libraries for firefox, xvfb, rvm, wget, nano, git and unzip.
RUN apt-get update && apt-get install -yf xvfb firefox software-properties-common wget nano git unzip 
RUN apt-add-repository -y ppa:rael-gc/rvm
RUN apt-get update && apt-get install rvm -y

# Upon installation is complete we need to install Ruby, Bundler and the Gemfile dependencies (selenium-cucumber for example).
RUN /bin/bash -l -c "source /usr/share/rvm/scripts/rvm"
RUN /bin/bash -l -c "rvm install ruby 2.4.1"
RUN /bin/bash -l -c "gem install bundler"
RUN /bin/bash -l -c "bundle install"

# And finally you'll need to download and setup Geckodriver
ENV GECKODRIVER_VERSION v0.19.1
RUN wget -P /tmp https://github.com/mozilla/geckodriver/releases/download/$GECKODRIVER_VERSION/geckodriver-$GECKODRIVER_VERSION-linux64.tar.gz
RUN tar -xvzf /tmp/geckodriver* -C /usr/share/rvm/rubies/ruby-2.4.1/bin/

# After building your image ($ docker build -t my-cucumber-base-image . )
# and running it detached and interactive ($ docker run cucumber-base -d -it )
# Go inside ($ docker exec -it imageId /bin/bash )
# So you can run: $ source /usr/share/rvm/scripts/rvm 
# You're all set to run your scripts: $ xvfb-run bash runner.sh
# Or: $ xvfb-run bash cucumber features/tests/login.feature -f pretty -f html -o report.html 
# Or, if you're running through rake tasks: $ xvfb-run rake execute:test_suite