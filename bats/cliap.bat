@echo off
cls

:MENU
ECHO ................SELECT AN OPTION......................
ECHO 1 - Run Test Suite 1
ECHO 2 - Run Regression Testing
ECHO 0 - EXIT
ECHO ...............................................

SET /P M=Choose an option and press enter:
IF %M%==1 GOTO TestSuite1
IF %M%==2 GOTO TestSuite2
IF %M%==0 GOTO EOF
ECHO Not Found
pause
CLS
GOTO MENU

:#......................................................
:TestSuite1
@echo on
rake test_suites:default
@echo off
pause
cls
GOTO MENU

:#......................................................
:TestSuite2
@echo on
ECHO "Running Regression Test Suite...."
ECHO "Just kidding. This is a TODO task :P"
@echo off
pause
cls
GOTO MENU

:#......................................................
:EOF
CLS
ECHO Bye
