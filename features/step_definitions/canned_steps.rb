
Then(/^I navigate to "\(\[\^\\"\]\*\)"$/) do |arg|
  pending
end

Then(/^I navigate forward$/) do
  pending
end

Then(/^I navigate back$/) do
  pending
end

Then(/^I refresh page$/) do
  pending
end

Then(/^I switch to new window$/) do
  pending
end

Then(/^I switch to previous window$/) do
  pending
end

Then(/^I switch to window having title "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I switch to window having url "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I close new window$/) do
  pending
end

Then(/^I switch to main window$/) do
  pending
end

Then(/^I switch to frame "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I switch to main content$/) do
  pending
end

Then(/^I resize browser window size to width \(\\d\+\) and height \(\\d\+\)$/) do
  pending
end

Then(/^I maximize browser window$/) do
  pending
end

Then(/^I close browser$/) do
  pending
end

Then(/^I zoom in page$/) do
  pending
end

Then(/^I zoom out page$/) do
  pending
end

Then(/^I zoom out page till I see element having id "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I zoom out page till I see element having name "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I zoom out page till I see element having class "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I zoom out page till I see element having xpath "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I zoom out page till I see element having css "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I reset page view$/) do
  pending
end

Then(/^I scroll to top of page$/) do
  pending
end

Then(/^I scroll to end of page$/) do
  pending
end

Then(/^I scroll to element having id "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I scroll to element having name "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I scroll to element having class "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I scroll to element having xpath "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I scroll to element having css "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I hover over element having id "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I hover over element having name "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I hover over element having class "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I hover over element having xpath "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I hover over element having css "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I should see page title as "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I should not see page title as "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I should see page title having partial text as "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I should not see page title having partial text as "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^element having id "\(\[\^\\"\]\*\)"([^"]*)"\(\.\*\?\)"$/) do |arg1, arg2|
  pending
end

Then(/^element having name "\(\[\^\\"\]\*\)"([^"]*)"\(\.\*\?\)"$/) do |arg1, arg2|
  pending
end

Then(/^element having class "\(\[\^\\"\]\*\)"([^"]*)"\(\.\*\?\)"$/) do |arg1, arg2|
  pending
end

Then(/^element having xpath "\(\[\^\\"\]\*\)"([^"]*)"\(\.\*\?\)"$/) do |arg1, arg2|
  pending
end

Then(/^element having css "\(\[\^\\"\]\*\)"([^"]*)"\(\.\*\?\)"$/) do |arg1, arg2|
  pending
end

Then(/^element having id "\(\[\^\\"\]\*\)"([^"]*)"\(\.\*\?\)"([^"]*)"\(\.\*\?\)"$/) do |arg1, arg2, arg3|
  pending
end

Then(/^element having name "\(\[\^\\"\]\*\)"([^"]*)"\(\.\*\?\)"([^"]*)"\(\.\*\?\)"$/) do |arg1, arg2, arg3|
  pending
end

Then(/^element having class "\(\[\^\\"\]\*\)"([^"]*)"\(\.\*\?\)"([^"]*)"\(\.\*\?\)"$/) do |arg1, arg2, arg3|
  pending
end

Then(/^element having xpath "\(\[\^\\"\]\*\)"([^"]*)"\(\.\*\?\)"([^"]*)"\(\.\*\?\)"$/) do |arg1, arg2, arg3|
  pending
end

Then(/^element having css "\(\[\^\\"\]\*\)"([^"]*)"\(\.\*\?\)"([^"]*)"\(\.\*\?\)"$/) do |arg1, arg2, arg3|
  pending
end

Then(/^element having id "\(\[\^\\"\]\*\)" should be enabled$/) do |arg|
  pending
end

Then(/^element having name "\(\[\^\\"\]\*\)" should be enabled$/) do |arg|
  pending
end

Then(/^element having class "\(\[\^\\"\]\*\)" should be enabled$/) do |arg|
  pending
end

Then(/^element having xpath "\(\[\^\\"\]\*\)" should be enabled$/) do |arg|
  pending
end

Then(/^element having css "\(\[\^\\"\]\*\)" should be enabled$/) do |arg|
  pending
end

Then(/^element having id "\(\[\^\\"\]\*\)" should be disabled$/) do |arg|
  pending
end

Then(/^element having name "\(\[\^\\"\]\*\)" should be disabled$/) do |arg|
  pending
end

Then(/^element having class "\(\[\^\\"\]\*\)" should be disabled$/) do |arg|
  pending
end

Then(/^element having xpath "\(\[\^\\"\]\*\)" should be disabled$/) do |arg|
  pending
end

Then(/^element having css "\(\[\^\\"\]\*\)" should be disabled$/) do |arg|
  pending
end

Then(/^element having id "\(\[\^\\"\]\*\)" should be present$/) do |arg|
  pending
end

Then(/^element having name "\(\[\^\\"\]\*\)" should be present$/) do |arg|
  pending
end

Then(/^element having class "\(\[\^\\"\]\*\)" should be present$/) do |arg|
  pending
end

Then(/^element having xpath "\(\[\^\\"\]\*\)" should be present$/) do |arg|
  pending
end

Then(/^element having css "\(\[\^\\"\]\*\)" should be present$/) do |arg|
  pending
end

Then(/^element having id "\(\[\^\\"\]\*\)" should not be present$/) do |arg|
  pending
end

Then(/^element having name "\(\[\^\\"\]\*\)" should not be present$/) do |arg|
  pending
end

Then(/^element having class "\(\[\^\\"\]\*\)" should not be present$/) do |arg|
  pending
end

Then(/^element having xpath "\(\[\^\\"\]\*\)" should not be present$/) do |arg|
  pending
end

Then(/^element having css "\(\[\^\\"\]\*\)" should not be present$/) do |arg|
  pending
end

Then(/^checkbox having id "\(\.\*\?\)" should be checked$/) do |arg|
  pending
end

Then(/^checkbox having name "\(\.\*\?\)" should be checked$/) do |arg|
  pending
end

Then(/^checkbox having class "\(\.\*\?\)" should be checked$/) do |arg|
  pending
end

Then(/^checkbox having xpath "\(\.\*\?\)" should be checked$/) do |arg|
  pending
end

Then(/^checkbox having css "\(\.\*\?\)" should be checked$/) do |arg|
  pending
end

Then(/^checkbox having id "\(\.\*\?\)" should be unchecked$/) do |arg|
  pending
end

Then(/^checkbox having name "\(\.\*\?\)" should be unchecked$/) do |arg|
  pending
end

Then(/^checkbox having class "\(\.\*\?\)" should be unchecked$/) do |arg|
  pending
end

Then(/^checkbox having xpath "\(\.\*\?\)" should be unchecked$/) do |arg|
  pending
end

Then(/^checkbox having css "\(\.\*\?\)" should be unchecked$/) do |arg|
  pending
end

Then(/^option "\(\.\*\?\)"([^"]*)"\(\.\*\?\)" should be selected$/) do |arg1, arg2|
  pending
end

Then(/^option "\(\.\*\?\)"([^"]*)"\(\.\*\?\)" should be unselected$/) do |arg1, arg2|
  pending
end

Then(/^radio button having id "\(\.\*\?\)" should be selected$/) do |arg|
  pending
end

Then(/^radio button having name "\(\.\*\?\)" should be selected$/) do |arg|
  pending
end

Then(/^radio button having class "\(\.\*\?\)" should be selected$/) do |arg|
  pending
end

Then(/^radio button having xpath "\(\.\*\?\)" should be selected$/) do |arg|
  pending
end

Then(/^radio button having css "\(\.\*\?\)" should be selected$/) do |arg|
  pending
end

Then(/^radio button having id "\(\.\*\?\)" should be unselected$/) do |arg|
  pending
end

Then(/^radio button having name "\(\.\*\?\)" should be unselected$/) do |arg|
  pending
end

Then(/^radio button having class "\(\.\*\?\)" should be unselected$/) do |arg|
  pending
end

Then(/^radio button having xpath "\(\.\*\?\)" should be unselected$/) do |arg|
  pending
end

Then(/^radio button having css "\(\.\*\?\)" should be unselected$/) do |arg|
  pending
end

Then(/^link having text "\(\.\*\?\)" should be present$/) do |arg|
  pending
end

Then(/^link having partial text "\(\.\*\?\)" should be present$/) do |arg|
  pending
end

Then(/^link having text "\(\.\*\?\)" should not be present$/) do |arg|
  pending
end

Then(/^link having partial text "\(\.\*\?\)" should not be present$/) do |arg|
  pending
end

Then(/^I should see alert text as "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^actual image having id "\(\.\*\?\)"([^"]*)"\(\.\*\?\)" should be similar$/) do |arg1, arg2|
  pending
end

Then(/^actual image having name "\(\.\*\?\)"([^"]*)"\(\.\*\?\)" should be similar$/) do |arg1, arg2|
  pending
end

Then(/^actual image having class "\(\.\*\?\)"([^"]*)"\(\.\*\?\)" should be similar$/) do |arg1, arg2|
  pending
end

Then(/^actual image having xpath "\(\.\*\?\)"([^"]*)"\(\.\*\?\)" should be similar$/) do |arg1, arg2|
  pending
end

Then(/^actual image having css "\(\.\*\?\)"([^"]*)"\(\.\*\?\)" should be similar$/) do |arg1, arg2|
  pending
end

Then(/^actual image having url "\(\.\*\?\)"([^"]*)"\(\.\*\?\)" should be similar$/) do |arg1, arg2|
  pending
end

Then(/^I enter "\(\[\^\\"\]\*\)"([^"]*)"\(\[\^\\"\]\*\)"$/) do |arg1, arg2|
  pending
end

Then(/^I clear input field having id "\(\[\^\\"\]\*\)"$/) do |arg|
  pending
end

Then(/^I clear input field having name "\(\[\^\\"\]\*\)"$/) do |arg|
  pending
end

Then(/^I clear input field having class "\(\[\^\\"\]\*\)"$/) do |arg|
  pending
end

Then(/^I clear input field having xpath "\(\[\^\\"\]\*\)"$/) do |arg|
  pending
end

Then(/^I clear input field having css "\(\[\^\\"\]\*\)"$/) do |arg|
  pending
end

Then(/^I select "\(\.\*\?\)"([^"]*)"\(\.\*\?\)"$/) do |arg1, arg2|
  pending
end

Then(/^I select \(\\d\+\) option by index from dropdown having id "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I select \(\\d\+\) option by index from dropdown having name "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I select \(\\d\+\) option by index from dropdown having class "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I select \(\\d\+\) option by index from dropdown having xpath "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I select \(\\d\+\) option by index from dropdown having css "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I select \(\\d\+\) option by index from multiselect dropdown having id "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I select \(\\d\+\) option by index from multiselect dropdown having name "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I select \(\\d\+\) option by index from multiselect dropdown having class "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I select \(\\d\+\) option by index from multiselect dropdown having xpath "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I select \(\\d\+\) option by index from multiselect dropdown having css "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I select all options from multiselect dropdown having id "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I select all options from multiselect dropdown having name "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I select all options from multiselect dropdown having class "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I select all options from multiselect dropdown having xpath "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I select all options from multiselect dropdown having css "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I unselect all options from mutliselect dropdown having id "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I unselect all options from mutliselect dropdown having name "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I unselect all options from mutliselect dropdown having class "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I unselect all options from mutliselect dropdown having xpath "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I unselect all options from mutliselect dropdown having css "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I check the checkbox having id "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I check the checkbox having name "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I check the checkbox having class "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I check the checkbox having xpath "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I check the checkbox having css "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I uncheck the checkbox having id "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I uncheck the checkbox having name "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I uncheck the checkbox having class "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I uncheck the checkbox having xpath "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I uncheck the checkbox having css "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I toggle checkbox having id "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I toggle checkbox having name "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I toggle checkbox having class "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I toggle checkbox having xpath "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I toggle checkbox having css "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I select radio button having id "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I select radio button having name "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I select radio button having class "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I select radio button having xpath "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I select radio button having css "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I click on element having id "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I click on element having name "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I click on element having class "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I click on element having xpath "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I click on element having css "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I click on element having id "\(\.\*\?\)"([^"]*)"\(\.\*\?\)"$/) do |arg1, arg2|
  pending
end

Then(/^I click on element having name "\(\.\*\?\)"([^"]*)"\(\.\*\?\)"$/) do |arg1, arg2|
  pending
end

Then(/^I click on element having class "\(\.\*\?\)"([^"]*)"\(\.\*\?\)"$/) do |arg1, arg2|
  pending
end

Then(/^I click on element having xpath "\(\.\*\?\)"([^"]*)"\(\.\*\?\)"$/) do |arg1, arg2|
  pending
end

Then(/^I click on element having css "\(\.\*\?\)"([^"]*)"\(\.\*\?\)"$/) do |arg1, arg2|
  pending
end

Then(/^I forcefully click on element having id "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I forcefully click on element having name "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I forcefully click on element having class "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I forcefully click on element having xpath "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I forcefully click on element having css "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I double click on element having id "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I double click on element having name "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I double click on element having class "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I double click on element having xpath "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I double click on element having css "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I click on link having text "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I click on link having partial text "\(\.\*\?\)"$/) do |arg|
  pending
end

Then(/^I wait for \(\\d\+\) sec$/) do
  pending
end

Then(/^I wait \(\\d\+\) seconds for element having id "\(\.\*\?\)" to display$/) do |arg|
  pending
end

Then(/^I wait \(\\d\+\) seconds for element having name "\(\.\*\?\)" to display$/) do |arg|
  pending
end

Then(/^I wait \(\\d\+\) seconds for element having class "\(\.\*\?\)" to display$/) do |arg|
  pending
end

Then(/^I wait \(\\d\+\) seconds for element having xpath "\(\.\*\?\)" to display$/) do |arg|
  pending
end

Then(/^I wait \(\\d\+\) seconds for element having css "\(\.\*\?\)" to display$/) do |arg|
  pending
end

Then(/^I wait \(\\d\+\) seconds for element having id "\(\.\*\?\)" to enable$/) do |arg|
  pending
end

Then(/^I wait \(\\d\+\) seconds for element having name "\(\.\*\?\)" to enable$/) do |arg|
  pending
end

Then(/^I wait \(\\d\+\) seconds for element having class "\(\.\*\?\)" to enable$/) do |arg|
  pending
end

Then(/^I wait \(\\d\+\) seconds for element having xpath "\(\.\*\?\)" to enable$/) do |arg|
  pending
end

Then(/^I wait \(\\d\+\) seconds for element having css "\(\.\*\?\)" to enable$/) do |arg|
  pending
end

Then(/^I accept alert$/) do
  pending
end

Then(/^I dismiss alert$/) do
  pending
end

Then(/^I take screenshot$/) do
  pending
end

Then(/^I print configuration$/) do
  pending
end