require 'rubygems'
require 'selenium-cucumber'

$DEBUG = false
$remote = false

# Store command line arguments
$browser_type = ENV['BROWSER'] || 'chrome'
$platform = ENV['PLATFORM'] || 'desktop'
$os_version = ENV['OS_VERSION']
$device_name = ENV['DEVICE_NAME']
$udid = ENV['UDID']
$app_path = ENV['APP_PATH']

# check for valid parameters
validate_parameters $platform, $browser_type, $app_path

# If platform is android or ios create driver instance for mobile browser
if $platform == 'android' or $platform == 'iOS'
  
  if $browser_type == 'native'
    $browser_type = "Browser"
  end
  
  if $platform == 'android'
    $device_name, $os_version = get_device_info
  end
  
  desired_caps = {
    caps:       {
      platformName:  $platform,
      browserName: $browser_type,
      versionNumber: $os_version,
      deviceName: $device_name,
      udid: $udid,
      app: ".//#{$app_path}"
      },
    }

  begin
    $driver = Appium::Driver.new(desired_caps).start_driver
  rescue Exception => e
    puts e.message
    Process.exit(0)  
  end
else # else create driver instance for desktop browser
  begin

    #To work with Selenium Grid in Docker
    if $browser_type == "chrome"
      caps = Selenium::WebDriver::Remote::Capabilities.chrome()
      caps['acceptInsecureCerts'] = true
    elsif $browser_type == "ff"
      caps = Selenium::WebDriver::Remote::Capabilities.firefox()
      caps['acceptInsecureCerts'] = true
    end

    if $remote
      $driver = Selenium::WebDriver.for :remote, url: 'http://localhost:4446/wd/hub', desired_capabilities: caps
    else
      $driver = Selenium::WebDriver.for(:"#{$browser_type}", desired_capabilities: caps)
    end
    #$driver = Selenium::WebDriver.for(:"#{$browser_type}")
    $driver.manage().window().maximize()
  rescue Exception => e
    puts e.message
    Process.exit(0)
  end
end
