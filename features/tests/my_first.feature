Feature: Login feature

  Scenario: As a valid user I can log into my web app
    Then I navigate to "https://www.google.com"
# Then I navigate forward
# Then I navigate back
# Then I refresh page
# Then I switch to new window
# Then I switch to previous window
# Then I switch to window having title "(.*?)"
# Then I switch to window having url "(.*?)"
# Then I close new window
# Then I switch to main window
# Then I switch to frame "(.*?)"
# Then I switch to main content
# Then I resize browser window size to width (\d+) and height (\d+)
# Then I maximize browser window
# Then I close browser
# Then I zoom in page
# Then I zoom out page
# Then I zoom out page till I see element having id "(.*?)"
# Then I zoom out page till I see element having name "(.*?)"
# Then I zoom out page till I see element having class "(.*?)"
# Then I zoom out page till I see element having xpath "(.*?)"
# Then I zoom out page till I see element having css "(.*?)"
# Then I reset page view
# Then I scroll to top of page
# Then I scroll to end of page
# Then I scroll to element having id "(.*?)"
# Then I scroll to element having name "(.*?)"
# Then I scroll to element having class "(.*?)"
# Then I scroll to element having xpath "(.*?)"
# Then I scroll to element having css "(.*?)"
# Then I hover over element having id "(.*?)"
# Then I hover over element having name "(.*?)"
# Then I hover over element having class "(.*?)"
# Then I hover over element having xpath "(.*?)"
# Then I hover over element having css "(.*?)"
# Then I should see page title as "(.*?)"
# Then I should not see page title as "(.*?)"

# Then I should see page title having partial text as "(.*?)"
# Then I should not see page title having partial text as "(.*?)"
# Then element having id "([^\"]*)" should have text as "(.*?)"
# Then element having name "([^\"]*)" should have text as "(.*?)"
# Then element having class "([^\"]*)" should have text as "(.*?)"
# Then element having xpath "([^\"]*)" should have text as "(.*?)"
# Then element having css "([^\"]*)" should have text as "(.*?)"

# Then element having id "([^\"]*)" should have partial text as "(.*?)"
# Then element having name "([^\"]*)" should have partial text as "(.*?)"
# Then element having class "([^\"]*)" should have partial text as "(.*?)"
# Then element having xpath "([^\"]*)" should have partial text as "(.*?)"
# Then element having css "([^\"]*)" should have partial text as "(.*?)"

# Then element having id "([^\"]*)" should not have text as "(.*?)"
# Then element having name "([^\"]*)" should not have text as "(.*?)"
# Then element having class "([^\"]*)" should not have text as "(.*?)"
# Then element having xpath "([^\"]*)" should not have text as "(.*?)"
# Then element having css "([^\"]*)" should not have text as "(.*?)"

# Then element having id "([^\"]*)" should not have partial text as "(.*?)"
# Then element having name "([^\"]*)" should not have partial text as "(.*?)"
# Then element having class "([^\"]*)" should not have partial text as "(.*?)"
# Then element having xpath "([^\"]*)" should not have partial text as "(.*?)"
# Then element having css "([^\"]*)" should not have partial text as "(.*?)"
# Then element having id "([^\"]*)" should have attribute "(.*?)" with value "(.*?)"
# Then element having name "([^\"]*)" should have attribute "(.*?)" with value "(.*?)"
# Then element having class "([^\"]*)" should have attribute "(.*?)" with value "(.*?)"
# Then element having xpath "([^\"]*)" should have attribute "(.*?)" with value "(.*?)"
# Then element having css "([^\"]*)" should have attribute "(.*?)" with value "(.*?)"

# Then element having id "([^\"]*)" should not have attribute "(.*?)" with value "(.*?)"
# Then element having name "([^\"]*)" should not have attribute "(.*?)" with value "(.*?)"
# Then element having class "([^\"]*)" should not have attribute "(.*?)" with value "(.*?)"
# Then element having xpath "([^\"]*)" should not have attribute "(.*?)" with value "(.*?)"
# Then element having css "([^\"]*)" should not have attribute "(.*?)" with value "(.*?)"
# Then element having id "([^\"]*)" should be enabled
# Then element having name "([^\"]*)" should be enabled
# Then element having class "([^\"]*)" should be enabled
# Then element having xpath "([^\"]*)" should be enabled
# Then element having css "([^\"]*)" should be enabled
# Then element having id "([^\"]*)" should be disabled
# Then element having name "([^\"]*)" should be disabled
# Then element having class "([^\"]*)" should be disabled
# Then element having xpath "([^\"]*)" should be disabled
# Then element having css "([^\"]*)" should be disabled
# Then element having id "([^\"]*)" should be present
# Then element having name "([^\"]*)" should be present
# Then element having class "([^\"]*)" should be present
# Then element having xpath "([^\"]*)" should be present
# Then element having css "([^\"]*)" should be present
# Then element having id "([^\"]*)" should not be present
# Then element having name "([^\"]*)" should not be present
# Then element having class "([^\"]*)" should not be present
# Then element having xpath "([^\"]*)" should not be present
# Then element having css "([^\"]*)" should not be present
# Then checkbox having id "(.*?)" should be checked
# Then checkbox having name "(.*?)" should be checked
# Then checkbox having class "(.*?)" should be checked
# Then checkbox having xpath "(.*?)" should be checked
# Then checkbox having css "(.*?)" should be checked
# Then checkbox having id "(.*?)" should be unchecked
# Then checkbox having name "(.*?)" should be unchecked
# Then checkbox having class "(.*?)" should be unchecked
# Then checkbox having xpath "(.*?)" should be unchecked
# Then checkbox having css "(.*?)" should be unchecked
# Then option "(.*?)" by text from dropdown having id "(.*?)" should be selected
# Then option "(.*?)" by text from dropdown having name "(.*?)" should be selected
# Then option "(.*?)" by text from dropdown having class "(.*?)" should be selected
# Then option "(.*?)" by text from dropdown having xpath "(.*?)" should be selected
# Then option "(.*?)" by text from dropdown having css "(.*?)" should be selected
# Then option "(.*?)" by value from dropdown having id "(.*?)" should be selected
# Then option "(.*?)" by value from dropdown having name "(.*?)" should be selected
# Then option "(.*?)" by value from dropdown having class "(.*?)" should be selected
# Then option "(.*?)" by value from dropdown having xpath "(.*?)" should be selected
# Then option "(.*?)" by value from dropdown having css "(.*?)" should be selected
# Then option "(.*?)" by text from dropdown having id "(.*?)" should be unselected
# Then option "(.*?)" by text from dropdown having name "(.*?)" should be unselected
# Then option "(.*?)" by text from dropdown having class "(.*?)" should be unselected
# Then option "(.*?)" by text from dropdown having xpath "(.*?)" should be unselected
# Then option "(.*?)" by text from dropdown having css "(.*?)" should be unselected
# Then option "(.*?)" by value from dropdown having id "(.*?)" should be unselected
# Then option "(.*?)" by value from dropdown having name "(.*?)" should be unselected
# Then option "(.*?)" by value from dropdown having class "(.*?)" should be unselected
# Then option "(.*?)" by value from dropdown having xpath "(.*?)" should be unselected
# Then option "(.*?)" by value from dropdown having css "(.*?)" should be unselected   
# Then radio button having id "(.*?)" should be selected
# Then radio button having name "(.*?)" should be selected
# Then radio button having class "(.*?)" should be selected
# Then radio button having xpath "(.*?)" should be selected
# Then radio button having css "(.*?)" should be selected
# Then radio button having id "(.*?)" should be unselected
# Then radio button having name "(.*?)" should be unselected
# Then radio button having class "(.*?)" should be unselected
# Then radio button having xpath "(.*?)" should be unselected
# Then radio button having css "(.*?)" should be unselected
# Then option "(.*?)" by text from radio button group having id "(.*?)" should be selected
# Then option "(.*?)" by text from radio button group having name "(.*?)" should be selected
# Then option "(.*?)" by text from radio button group having class "(.*?)" should be selected
# Then option "(.*?)" by text from radio button group having xpath "(.*?)" should be selected
# Then option "(.*?)" by text from radio button group having css "(.*?)" should be selected
# Then option "(.*?)" by value from radio button group having id "(.*?)" should be selected
# Then option "(.*?)" by value from radio button group having name "(.*?)" should be selected
# Then option "(.*?)" by value from radio button group having class "(.*?)" should be selected
# Then option "(.*?)" by value from radio button group having xpath "(.*?)" should be selected
# Then option "(.*?)" by value from radio button group having css "(.*?)" should be selected
# Then option "(.*?)" by text from radio button group having id "(.*?)" should be unselected
# Then option "(.*?)" by text from radio button group having name "(.*?)" should be unselected
# Then option "(.*?)" by text from radio button group having class "(.*?)" should be unselected
# Then option "(.*?)" by text from radio button group having xpath "(.*?)" should be unselected
# Then option "(.*?)" by text from radio button group having css "(.*?)" should be unselected
# Then option "(.*?)" by value from radio button group having id "(.*?)" should be unselected
# Then option "(.*?)" by value from radio button group having name "(.*?)" should be unselected
# Then option "(.*?)" by value from radio button group having class "(.*?)" should be unselected
# Then option "(.*?)" by value from radio button group having xpath "(.*?)" should be unselected
# Then option "(.*?)" by value from radio button group having css "(.*?)" should be unselected
# Then link having text "(.*?)" should be present
# Then link having partial text "(.*?)" should be present
# Then link having text "(.*?)" should not be present
# Then link having partial text "(.*?)" should not be present
# Then I should see alert text as "(.*?)"
# Then actual image having id "(.*?)" and expected image having url "(.*?)" should be similar
# Then actual image having name "(.*?)" and expected image having url "(.*?)" should be similar
# Then actual image having class "(.*?)" and expected image having url "(.*?)" should be similar
# Then actual image having xpath "(.*?)" and expected image having url "(.*?)" should be similar
# Then actual image having css "(.*?)" and expected image having url "(.*?)" should be similar
# Then actual image having url "(.*?)" and expected image having url "(.*?)" should be similar
# Then actual image having id "(.*?)" and expected image having image_name "(.*?)" should be similar
# Then actual image having name "(.*?)" and expected image having image_name "(.*?)" should be similar
# Then actual image having class "(.*?)" and expected image having image_name "(.*?)" should be similar
# Then actual image having xpath "(.*?)" and expected image having image_name "(.*?)" should be similar
# Then actual image having css "(.*?)" and expected image having image_name "(.*?)" should be similar
# Then actual image having url "(.*?)" and expected image having image_name "(.*?)" should be similar
# Then actual image having id "(.*?)" and expected image having id "(.*?)" should be similar
# Then actual image having name "(.*?)" and expected image having name "(.*?)" should be similar
# Then actual image having class "(.*?)" and expected image having class "(.*?)" should be similar
# Then actual image having xpath "(.*?)" and expected image having xpath "(.*?)" should be similar
# Then actual image having css "(.*?)" and expected image having css "(.*?)" should be similar
# Then actual image having url "(.*?)" and expected image having url "(.*?)" should be similar
# Then I enter "([^\"]*)" into input field having id "([^\"]*)"
# Then I enter "([^\"]*)" into input field having name "([^\"]*)"
# Then I enter "([^\"]*)" into input field having class "([^\"]*)"
# Then I enter "([^\"]*)" into input field having xpath "([^\"]*)"
# Then I enter "([^\"]*)" into input field having css "([^\"]*)"
# Then I clear input field having id "([^\"]*)"
# Then I clear input field having name "([^\"]*)"
# Then I clear input field having class "([^\"]*)" 
# Then I clear input field having xpath "([^\"]*)"
# Then I clear input field having css "([^\"]*)"
# Then I select "(.*?)" option by text from dropdown having id "(.*?)"
# Then I select "(.*?)" option by text from dropdown having name "(.*?)"
# Then I select "(.*?)" option by text from dropdown having class "(.*?)"
# Then I select "(.*?)" option by text from dropdown having xpath "(.*?)"
# Then I select "(.*?)" option by text from dropdown having css "(.*?)"
# Then I select (\d+) option by index from dropdown having id "(.*?)"
# Then I select (\d+) option by index from dropdown having name "(.*?)"
# Then I select (\d+) option by index from dropdown having class "(.*?)"
# Then I select (\d+) option by index from dropdown having xpath "(.*?)"
# Then I select (\d+) option by index from dropdown having css "(.*?)"  
# Then I select "(.*?)" option by value from dropdown having id "(.*?)"
# Then I select "(.*?)" option by value from dropdown having name "(.*?)"
# Then I select "(.*?)" option by value from dropdown having class "(.*?)"
# Then I select "(.*?)" option by value from dropdown having xpath "(.*?)"
# Then I select "(.*?)" option by value from dropdown having css "(.*?)"
# Then I select "(.*?)" option by text from multiselect dropdown having id "(.*?)"
# Then I select "(.*?)" option by text from multiselect dropdown having name "(.*?)"
# Then I select "(.*?)" option by text from multiselect dropdown having class "(.*?)"
# Then I select "(.*?)" option by text from multiselect dropdown having xpath "(.*?)"
# Then I select "(.*?)" option by text from multiselect dropdown having css "(.*?)"
# Then I select (\d+) option by index from multiselect dropdown having id "(.*?)"
# Then I select (\d+) option by index from multiselect dropdown having name "(.*?)"
# Then I select (\d+) option by index from multiselect dropdown having class "(.*?)"
# Then I select (\d+) option by index from multiselect dropdown having xpath "(.*?)"
# Then I select (\d+) option by index from multiselect dropdown having css "(.*?)"
# Then I select "(.*?)" option by value from multiselect dropdown having id "(.*?)"
# Then I select "(.*?)" option by value from multiselect dropdown having name "(.*?)"
# Then I select "(.*?)" option by value from multiselect dropdown having class "(.*?)"
# Then I select "(.*?)" option by value from multiselect dropdown having xpath "(.*?)"
# Then I select "(.*?)" option by value from multiselect dropdown having css "(.*?)"
# Then I select all options from multiselect dropdown having id "(.*?)"
# Then I select all options from multiselect dropdown having name "(.*?)"
# Then I select all options from multiselect dropdown having class "(.*?)"
# Then I select all options from multiselect dropdown having xpath "(.*?)"
# Then I select all options from multiselect dropdown having css "(.*?)"
# Then I unselect all options from mutliselect dropdown having id "(.*?)"
# Then I unselect all options from mutliselect dropdown having name "(.*?)"
# Then I unselect all options from mutliselect dropdown having class "(.*?)"
# Then I unselect all options from mutliselect dropdown having xpath "(.*?)"
# Then I unselect all options from mutliselect dropdown having css "(.*?)"
# Then I check the checkbox having id "(.*?)"
# Then I check the checkbox having name "(.*?)"
# Then I check the checkbox having class "(.*?)"
# Then I check the checkbox having xpath "(.*?)"
# Then I check the checkbox having css "(.*?)"
# Then I uncheck the checkbox having id "(.*?)"
# Then I uncheck the checkbox having name "(.*?)"
# Then I uncheck the checkbox having class "(.*?)"
# Then I uncheck the checkbox having xpath "(.*?)"
# Then I uncheck the checkbox having css "(.*?)"
# Then I toggle checkbox having id "(.*?)"
# Then I toggle checkbox having name "(.*?)"
# Then I toggle checkbox having class "(.*?)"
# Then I toggle checkbox having xpath "(.*?)"
# Then I toggle checkbox having css "(.*?)"
# Then I select radio button having id "(.*?)"
# Then I select radio button having name "(.*?)"
# Then I select radio button having class "(.*?)"
# Then I select radio button having xpath "(.*?)"
# Then I select radio button having css "(.*?)"
# Then I select "(.*?)" option by text from radio button group having id "(.*?)"
# Then I select "(.*?)" option by text from radio button group having name "(.*?)"
# Then I select "(.*?)" option by text from radio button group having class "(.*?)"
# Then I select "(.*?)" option by text from radio button group having xpath "(.*?)"
# Then I select "(.*?)" option by text from radio button group having css "(.*?)"
# Then I select "(.*?)" option by value from radio button group having id "(.*?)"
# Then I select "(.*?)" option by value from radio button group having name "(.*?)"
# Then I select "(.*?)" option by value from radio button group having class "(.*?)"
# Then I select "(.*?)" option by value from radio button group having xpath "(.*?)"
# Then I select "(.*?)" option by value from radio button group having css "(.*?)"
# Then I click on element having id "(.*?)"
# Then I click on element having name "(.*?)"
# Then I click on element having class "(.*?)"
# Then I click on element having xpath "(.*?)"
# Then I click on element having css "(.*?)"
# Then I click on element having id "(.*?)" and text "(.*?)"
# Then I click on element having name "(.*?)" and text "(.*?)"
# Then I click on element having class "(.*?)" and text "(.*?)"
# Then I click on element having xpath "(.*?)" and text "(.*?)"
# Then I click on element having css "(.*?)" and text "(.*?)" 
# Then I forcefully click on element having id "(.*?)"
# Then I forcefully click on element having name "(.*?)"
# Then I forcefully click on element having class "(.*?)"
# Then I forcefully click on element having xpath "(.*?)"
# Then I forcefully click on element having css "(.*?)"
# Then I double click on element having id "(.*?)"
# Then I double click on element having name "(.*?)"
# Then I double click on element having class "(.*?)"
# Then I double click on element having xpath "(.*?)"
# Then I double click on element having css "(.*?)"
# Then I click on link having text "(.*?)"
# Then I click on link having partial text "(.*?)"
# Then I wait for (\d+) sec
# Then I wait (\d+) seconds for element having id "(.*?)" to display
# Then I wait (\d+) seconds for element having name "(.*?)" to display
# Then I wait (\d+) seconds for element having class "(.*?)" to display
# Then I wait (\d+) seconds for element having xpath "(.*?)" to display
# Then I wait (\d+) seconds for element having css "(.*?)" to display
# Then I wait (\d+) seconds for element having id "(.*?)" to enable
# Then I wait (\d+) seconds for element having name "(.*?)" to enable
# Then I wait (\d+) seconds for element having class "(.*?)" to enable
# Then I wait (\d+) seconds for element having xpath "(.*?)" to enable
# Then I wait (\d+) seconds for element having css "(.*?)" to enable
# Then I accept alert 
# Then I dismiss alert
# Then I take screenshot
# Then I print configuration
# Then I tap on element having id "(.*?)"
# Then I tap on element having name "(.*?)"
# Then I tap on element having class "(.*?)"
# Then I tap on element having xpath "(.*?)"
# Then I tap on element having css "(.*?)"
# Then I tap on back button of device
# Then I press back button of device
# Then I swipe from element having id "(.*?)" to element having id "(.*?)"
# Then I swipe from element having id "(.*?)" to element having name "(.*?)"
# Then I swipe from element having id "(.*?)" to element having class "(.*?)"
# Then I swipe from element having id "(.*?)" to element having xpath "(.*?)"

# Then I swipe from element having name "(.*?)" to element having id "(.*?)"
# Then I swipe from element having name "(.*?)" to element having name "(.*?)"
# Then I swipe from element having name "(.*?)" to element having class "(.*?)"
# Then I swipe from element having name "(.*?)" to element having xpath "(.*?)"

# Then I swipe from element having class "(.*?)" to element having id "(.*?)"
# Then I swipe from element having class "(.*?)" to element having name "(.*?)"
# Then I swipe from element having class "(.*?)" to element having class "(.*?)"
# Then I swipe from element having class "(.*?)" to element having xpath "(.*?)"

# Then I swipe from element having xpath "(.*?)" to element having id "(.*?)"
# Then I swipe from element having xpath "(.*?)" to element having name "(.*?)"
# Then I swipe from element having xpath "(.*?)" to element having class "(.*?)"
# Then I swipe from element having xpath "(.*?)" to element having xpath "(.*?)"
# Then I swipe from co-ordinates "(.*?)","(.*?)" to co-ordinates "(.*?)","(.*?)"
# Then I swipe right
# Then I swipe left
# Then I swipe up
# Then I swipe down
# Then I swipe element having id "(.*?)" to right
# Then I swipe element having name "(.*?)" to right
# Then I swipe element having class "(.*?)" to right
# Then I swipe element having xpath "(.*?)" to right

# Then I swipe element having id "(.*?)" to left
# Then I swipe element having name "(.*?)" to left
# Then I swipe element having class "(.*?)" to left
# Then I swipe element having xpath "(.*?)" to left

# Then I swipe element having id "(.*?)" to up
# Then I swipe element having name "(.*?)" to up
# Then I swipe element having class "(.*?)" to up
# Then I swipe element having xpath "(.*?)" to up

# Then I swipe element having id "(.*?)" to down
# Then I swipe element having name "(.*?)" to down
# Then I swipe element having class "(.*?)" to down
# Then I swipe element having xpath "(.*?)" to down
# Then I swipe co-ordinates "(.*?)","(.*?)" to left
# Then I swipe co-ordinates "(.*?)","(.*?)" to right
# Then I swipe co-ordinates "(.*?)","(.*?)" to up
# Then I swipe co-ordinates "(.*?)","(.*?)" to down
# Then I long tap on element having id "(.*?)"
# Then I long tap on element having name "(.*?)"
# Then I long tap on element having class "(.*?)"
# Then I long tap on element having xpath "(.*?)"
# Then I long tap on element having id "(.*?)" for "(.*?)" sec
# Then I long tap on element having name "(.*?)" for "(.*?)" sec
# Then I long tap on element having class "(.*?)" for "(.*?)" sec
# Then I long tap on element having xpath "(.*?)" for "(.*?)" sec
# Then I long tap on co\-ordinate "(.*?)","(.*?)"
# Then I long tap on co\-ordinate "(.*?)","(.*?)" for "(.*?)" sec
# Then I close app