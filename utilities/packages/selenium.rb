module Selenium

    class Overrides
        def initialize
            
        end

        def self.ShowServerInfoFrame(serverinfo)
            script = "var new_frame = document.createElement('DIV'); " +
                "new_frame.setAttribute('id', 'serverinfo'); " +
                "new_frame.setAttribute('style', 'background: Yellow; border: 2px solid black; max-width: 300px; position: absolute; left: 20%; top:40%; padding: 10%;'); "+
                "new_frame.innerHTML = 'Something went wrong at: #{serverinfo}. Please Verify!'; "+
                "document.body.appendChild(new_frame); "
            Overrides.JavaScriptExecutor(script)
        end

        def self.take_screenshot_of_failed_scenario(scenario, serverinfo)
            feat = scenario.feature
            formatted = feat.file[/\s*([A-Z]|_|[0-9]|_)+.feature\s*/i].slice! /\s*([A-Z]|_|[0-9]|_)]+\s*/i
            cur_time = Time.now.strftime('%Y%m%d%H%M%S%L')
            current_dir = Dir.pwd
            FileUtils.cd("..")
            Overrides.ShowServerInfoFrame(serverinfo)
            img_path = "./utilities/test_results/screenshots/#{formatted}_" + cur_time + ".png"
            $driver.save_screenshot(img_path)
            dir_for_image = Dir.pwd
            FileUtils.cd("..")
            Dir.chdir(dir_for_image)
            Dir.chdir(current_dir)
            dir_for_image + "/utilities/test_results/screenshots/#{formatted}_" + cur_time  + ".png"
        end

        def self.JavaScriptExecutor(script, *args)
            $driver.execute_script(script, *args)
        end

        def self.HighlightElement(web_element)
            Overrides.JavaScriptExecutor("arguments[0].setAttribute('style', 'background: GreenYellow; border: 3px solid DodgerBlue;');", web_element)
        end

        def isElementExist(locator, path)
            wait = Selenium::WebDriver::Wait.new(:timeout => 30)
            wait.until{
                web_element = $driver.find_element(:"#{locator}", "#{path}")
                if (web_element.displayed?)
                    Overrides.HighlightElement(web_element)
                    true
                else
                    false
                end
            }
        end

        def self.get_element_texts(locatorHash)
            if(locatorHash.size > 0)
                locatorHash.each{|locator, path|
                    if(self.new.isElementExist(locator, path))
                        return get_element_text(locator, path)
                    end
                }
            end
        end
    end
end