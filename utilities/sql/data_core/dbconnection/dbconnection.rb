require 'tiny_tds'
require_relative '../enums/enums'
require_relative '../entities/entities'

module DBConnection
    class MSSQL

        attr_accessor :client, :username, :password, :host, :database, :login_timeout, :timeout

        def initialize
            @username = ""
            @password = ""
            @port = 1433
            @database = "Defautl DB"
            @login_timeout = 60
            @timeout = 30
        end

        if ($testEnvironment == "QA")
            @host = SQLEnums::Servers::QA_DB_SERVER
        elsif ($testEnvironment == "Staging")
            @host = SQLEnums::Servers::STAGING_DB_SERVER
        elsif ($testEnvironment == "Production")
            @host = SQLEnums::Servers::PRODUCTION_DB_SERVER
        else
            @host = SQLEnums::Servers::QA_DB_SERVER
        end

        def getConnection()
            begin
                if(@client == nil)
                    @client = TinyTds::Client.new username: @username, password: @password, host: @host,
                            port: @port, database: @database, login_timeout: @login_timeout, timeout: @timeout
                else (@client.is_a?(TinyTds::Client) && @client.active? == false)
                    @client = TinyTds::Client.new username: @username, password: @password, host: @host,
                    port: @port, database: @database, login_timeout: @login_timeout, timeout: @timeout
                end
            rescue Exception => exception
                puts exception.message
            end
        end

        def execute(sql)
            begin
                result = @client.execute(sql)
            rescue Exception => exception
                p exception.message
            end
        end

        def closeConnection()
            begin
                if(@client != nil)
                    @client.close
                end
            rescue Exception => exception
                p "Check the closeConnection class"
                p exception.message
            end
        end
    end
end