require_relative '../data_core/dbconnection/dbconnection'

module SQLQueries
    class Person
        def get_one_person_through_entities()
            begin
                sqlInstance = DBConnection::MSSQL.new()
                con = sqlInstance.getConnection()
                dataset = con.execute("Select top 1 PersonID from Persona")
                restults = dataset.each
                personas = Array.new(dataset.affected_rows)
                for i in 0...dataset.affected_rows
                    ps = Entities::Person.new()
                    ps.personid = results[i]["PersonID"]
                    #personas[i] = ps
                    personas.push(ps)
                end
                personas
            rescue Exception => exception
                p exception.message()
            ensure
                sqlInstance.closeConnection()
            end
        end

        def get_one_person_through_resultset()
            begin
                sqlInstance = DBConnection::MSSQL.new()
                dataset = sqlInstance.getConnection().execute("Select top 1 PersonID from Persona")
                results = dataset.each
                results[0]["PersonID"]
            rescue Exception => exception
                p exception.message()
            ensure
                sqlInstance.closeConnection()
            end
        end
    end
end